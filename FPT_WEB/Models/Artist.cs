﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FPT_WEB.Models
{
    public class Artist
    {
        public int ArtistID { get; set; }
        [Required]
        [Display(Name = "Artist Name")]
        public string ArtistName { get; set; }
        [Required]
        [Display(Name = "Album Name")]
        public string AlbumName { get; set; }
        public string ImageURL { get; set; }
        [Required]
        [Display(Name = "Date Release")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime ReleaseDate { get; set; }
        [Required]
        [Display(Name = "Price")]
        public decimal Price { get; set; }
        [Display(Name = "Play Track")]
        public string SampleURL { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}