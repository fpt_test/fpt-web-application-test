﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPT_WEB.Models
{
    public class YoutubeResult
    {
        public string nextPageToken { get; set; }
        public string previousPageToken { get; set; }
        public string videoId { get; set; }
        public int totalResult { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string title { get; set; }
        public string ImageURL { get; set; }
        public string artistName { get; set; }
    }
}