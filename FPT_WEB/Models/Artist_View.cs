﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FPT_WEB.Models
{
    public class Artist_View
    {
        [Required]
        [Display(Name = "Artist Name")]
        public string ArtistName { get; set; }

        [Required]
        [Display(Name = "Album/Title")]
        public string AlbumName { get; set; }

        [Display(Name = "Image(URL)")]
        public string ImageURL { get; set; }

        [Required]
        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Currency")]
        public string Currency { get; set; }

        [Display(Name = "Video/Audio Source (URL)")]
        public string SampleURL { get; set; }
    }
}