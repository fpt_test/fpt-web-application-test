﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FPT_WEB.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebGrease;

namespace FPT_WEB.Controllers
{
    public class ArtistController : Controller
    {
        public static string urlEndPoint = "http://localhost:54008/api/";
        private static HttpClient client = new HttpClient();

        // GET: Artist
        public async Task<ActionResult> Index()
        {
            List<Artist> listArtist = new List<Artist>();
            try
            {
                Artist_View viewArtist = new Artist_View();
                string response = await CallAPI(urlEndPoint + "Artists", "GET", null);
                JArray result = JsonConvert.DeserializeObject<JArray>(response);

                for (int i = 0; i < result.Count; i++)
                {
                    Artist data = new Artist();
                    data = JsonConvert.DeserializeObject<Artist>(result[i].ToString());
                    listArtist.Add(data);
                }
            }
            catch (Exception e)
            {
                ViewData["error_Initiate"] = e.Message;
                listArtist = new List<Artist>();
            }

            return View(listArtist);
        }

        // GET: Artist/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [Route("Artist/Create")]
        public ActionResult Create(Artist_View dataInput)
        {
            try
            {
                // TODO: Add insert logic here
                return new PartialViewResult
                {
                    ViewName = "CreateArtistPartial",
                    ViewData = new ViewDataDictionary<Artist_View>(new Artist_View{ })
                };
            }
            catch
            {
                return View("Index");
            }
        }

        [Route("Artist/Create")]
        [HttpPost]
        public async Task<ActionResult> OnPostCreateArtistPartial(Artist_View model)
        {
            if (ModelState.IsValid)
            {
                Artist data = new Artist();
                data.AlbumName = model.AlbumName;
                data.ArtistName = model.ArtistName;
                data.ImageURL = model.ImageURL;
                data.Price = model.Price;
                data.ReleaseDate = DateTime.Now;
                data.LastUpdated = DateTime.Now;
                data.SampleURL = model.SampleURL;

                using (var client = new HttpClient())
                {
                    var parameter = new List<KeyValuePair<string, string>>()
                    {
                        new KeyValuePair<string, string>("ArtistName", data.ArtistName),
                        new KeyValuePair<string, string>("AlbumName", data.AlbumName),
                        new KeyValuePair<string, string>("ImageURL", data.ImageURL),
                        new KeyValuePair<string, string>("ReleaseDate", data.ReleaseDate.ToString()),
                        new KeyValuePair<string, string>("LastUpdated", data.LastUpdated.ToString()),
                        new KeyValuePair<string, string>("Price", data.Price.ToString()),
                        new KeyValuePair<string, string>("SampleURL", data.SampleURL)
                    };

                    var content = new FormUrlEncodedContent(parameter);
                    string url = urlEndPoint + "Artists";
                    var result = await CallAPI(url, "POST", content);

                    if (result.Contains("The request is invalid."))
                    {
                        return new PartialViewResult
                        {
                            ViewName = "CreateArtistPartial",
                            ViewData = new ViewDataDictionary<Artist_View>(ViewData)
                        };
                    }
                }
            }
            else
            {
                return new PartialViewResult
                {
                    ViewName = "CreateArtistPartial",
                    ViewData = new ViewDataDictionary<Artist_View>(ViewData)
                };
            }

            return RedirectToAction("Index", "Artist");
        }

        [Route("Artist/Youtube")]
        public ActionResult Youtube(string keySearch)
        {
            try
            {
                // TODO: Add insert logic here
                return new PartialViewResult
                {
                    ViewName = "YoutubeSearchView",
                    ViewData = new ViewDataDictionary<IEnumerable<YoutubeResult>>(new List<YoutubeResult>(){ })
                };
            }
            catch
            {
                return View("Index");
            }
        }

        [HttpPost]
        [Route("Artist/Youtube")]
        public async Task<ActionResult> YoutubeSearch(string searchkey, string tokenPage)
        {
            List<YoutubeResult> listData = new List<YoutubeResult>(); 
            string pageAccess = string.Empty;
            if (!string.IsNullOrEmpty(searchkey))
            {
                pageAccess = string.IsNullOrEmpty(tokenPage) ? string.Empty : "&tokenPage=" + tokenPage;
                string url = urlEndPoint + "youtube" + "?keySearch=" + searchkey + pageAccess;
                var response = await CallAPI(url, "POST", null);

                JArray list = JsonConvert.DeserializeObject<JArray>(response);

                if (list != null)
                {
                    if (list.Count > 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            try
                            {
                                YoutubeResult ytb = new YoutubeResult();
                                ytb = JsonConvert.DeserializeObject<YoutubeResult>(list[i].ToString());
                                listData.Add(ytb);
                            }
                            catch (Exception e)
                            {
                                return null;
                            }
                        }
                    }
                }
            }

            return new PartialViewResult
            {
                ViewName = "YoutubeSearchView",
                ViewData = new ViewDataDictionary<IEnumerable<YoutubeResult>>(listData)
            };
        }

        [HttpPost]
        [Route("Artist/Youtube")]
        public async Task<ActionResult> YoutubeAddItem(string jsonData)
        {
            if (!string.IsNullOrEmpty(jsonData))
            {
                YoutubeResult data = new YoutubeResult();
                Artist insert = new Artist();
                data = JsonConvert.DeserializeObject<YoutubeResult>(jsonData);
                insert.AlbumName = data.title;
                insert.ArtistName = data.artistName;
                insert.ImageURL = data.ImageURL;
                insert.Price = 5;
                insert.ReleaseDate = data.ReleaseDate;
                insert.LastUpdated = data.ReleaseDate;
                insert.SampleURL = "https://www.youtube.com/embed/" + data.videoId;

                using (var client = new HttpClient())
                {
                    var parameter = new List<KeyValuePair<string, string>>()
                    {
                        new KeyValuePair<string, string>("ArtistName", insert.ArtistName),
                        new KeyValuePair<string, string>("AlbumName", insert.AlbumName),
                        new KeyValuePair<string, string>("ImageURL", insert.ImageURL),
                        new KeyValuePair<string, string>("ReleaseDate", insert.ReleaseDate.ToString()),
                        new KeyValuePair<string, string>("LastUpdated", insert.LastUpdated.ToString()),
                        new KeyValuePair<string, string>("Price", insert.Price.ToString()),
                        new KeyValuePair<string, string>("SampleURL", insert.SampleURL)
                    };

                    var content = new FormUrlEncodedContent(parameter);
                    string url = urlEndPoint + "Artists";
                    var result = await CallAPI(url, "POST", content);

                    if (result.Contains("The request is invalid."))
                    {
                        return new PartialViewResult
                        {
                            ViewName = "YoutubeSearchView",
                            ViewData = new ViewDataDictionary<IEnumerable<YoutubeResult>>(new List<YoutubeResult>() { })
                        };
                    }
                }


            }

            return RedirectToAction("Index", "Artist");
        }

        [Route("Artist/Edit")]
        public async Task<ActionResult> Edit(int id)
        {
            string url = urlEndPoint + "Artists/" + id.ToString();
            var response = await CallAPI(url, "GET", null);

            Artist data = new Artist();
            data = JsonConvert.DeserializeObject<Artist>(response);

            return new PartialViewResult
            {
                ViewName = "EditArtistPartial",
                ViewData = new ViewDataDictionary<Artist>(data)
            };
        }

        [HttpPost]
        [Route("Artist/Edit")]
        public async Task<ActionResult> PostEdit(Artist edited)
        {
            try
            {
                Artist dataEntry = new Artist();
                string url = urlEndPoint + "Artists/" + edited.ArtistID.ToString();

                if (ModelState.IsValid)
                {
                    var responseEntry = await CallAPI(url, "GET", null);
                    dataEntry = JsonConvert.DeserializeObject<Artist>(responseEntry);

                    using (var client = new HttpClient())
                    {
                        var parameter = new List<KeyValuePair<string, string>>()
                        {
                            new KeyValuePair<string, string>("ArtistID", edited.ArtistID.ToString()),
                            new KeyValuePair<string, string>("ArtistName", edited.ArtistName),
                            new KeyValuePair<string, string>("AlbumName", edited.AlbumName),
                            new KeyValuePair<string, string>("ImageURL", edited.ImageURL),
                            new KeyValuePair<string, string>("ReleaseDate", dataEntry.ReleaseDate.ToString()),
                            new KeyValuePair<string, string>("LastUpdated", DateTime.Now.ToString()),
                            new KeyValuePair<string, string>("Price", edited.Price.ToString()),
                            new KeyValuePair<string, string>("SampleURL", edited.SampleURL)
                        };

                        var content = new FormUrlEncodedContent(parameter);
                        var response = await CallAPI(url, "PUT", content);

                        if (response.Contains("The request is invalid."))
                        {
                            return new PartialViewResult
                            {
                                ViewName = "EditArtistPartial",
                                ViewData = new ViewDataDictionary<Artist_View>(ViewData)
                            };
                        }
                    }
                }
                else
                {
                    return new PartialViewResult
                    {
                        ViewName = "EditArtistPartial",
                        ViewData = new ViewDataDictionary<Artist_View>(ViewData)
                    };
                }
            }
            catch(Exception e)
            {
                Console.Write(e.Message);
                throw;
            }

            return RedirectToAction("Index", "Artist");
        }

        [Route("Artist/Delete")]
        public async Task<ActionResult> Delete(int id)
        {
            string url = urlEndPoint + "Artists/" + id.ToString();
            var response = await CallAPI(url, "DELETE", null);

            return RedirectToAction("Index", "Artist");
        }

        private static async Task<string> CallAPI(string url, string httpMethod, FormUrlEncodedContent content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            string getResponse = string.Empty;
            if (httpMethod.Equals("POST"))
                response = await client.PostAsync(url, content);
            else if(httpMethod.Equals("GET"))
                response = client.GetAsync(url).Result;
            else if(httpMethod.Equals("PUT"))
                response = await client.PutAsync(url, content);
            else
                response = await client.DeleteAsync(url);

            if (httpMethod == "DELETE")
                getResponse = response.StatusCode.ToString();
            else
                getResponse = await response.Content.ReadAsStringAsync();

            return getResponse;
        }
    }
}
